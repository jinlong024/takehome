var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');


// define
var userSchema = mongoose.Schema({
        email: String,
        password: String,
        name: String,
        amount_USD: Number,
        amount_YUAN: Number,
        amount_EURO: Number,
        history: []

    },
    {collection: 'user'}

);
// generating a hash
userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', userSchema);