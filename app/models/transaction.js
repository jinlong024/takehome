var mongoose = require('mongoose');

// define
var transactionSchema = mongoose.Schema({
    from_email   : String,
    to_email     : String,
    amount       : Number,
    currency     : String,
    text         : String,
    type         : String,
    date         : Date
});

module.exports = mongoose.model('Trans', transactionSchema);