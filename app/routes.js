var mongoose = require('mongoose');
var Trans = require('../app/models/transaction');
var User = require('../app/models/user');


module.exports = function (app, passport) {


//direct to login
    app.get('/', function (req, res) {
        res.render('index.ejs');
    });

//direct to homepage

    app.get('/homepage', function (req, res) {
        res.render('homepage.ejs')
        console.log(req.user.email);
    })

//direct to sendpage
    app.get('/send', function (req, res) {
        res.render('send.ejs');
    });

// post send information, store both in user & transaction db
    app.post('/app_request', function (req, res) {
        var request = new Trans();
        money       = req.param('currency');

        request.from_email = req.user.email;
        request.to_email = req.param('email');
        request.amount = req.param('amount');
        request.currency = req.param('currency');
        request.text = req.param('message');
        request.type = req.param('type');
        request.date = new Date();
        request.save();

// find and update login user info (amount and history) using find req
      User.findOne({email:req.user.email},function(err,from){

/*
         if (err)
           console.log('can not find login user')
          else{
*/

             if(money == 'USD'){
                 from.amount_USD = from.amount_USD - req.param('amount');
                 from.history.push(request._id, request.date);

                 from.save(function(err) {
                     if (err)
                         console.log('error')
                     else
                         console.log('success')
                 });

             }
             if (money == 'YUAN'){
             from.amount_YUAN = from.amount_YUAN - req.param('amount');
                 from.history.push(request._id, request.date);

                 from.save(function(err) {
                     if (err)
                         console.log('error')
                     else
                         console.log('success')
                 });

             }
             else if (money == 'EURO')
          from.amount_EURO = from.amount_EURO - req.param('amount');
          from.history.push(request._id, request.date);

          from.save(function(err) {
              if (err)
                  console.log('error')
              else
                  console.log('success')
          });
          console.log(from.amount);

//        }
      });


        // find and update receiver (to) user info (amount and history) using find req
        User.findOne({email:req.param('email')},function(err,to){



            if(money == 'USD'){
                to.amount_USD = to.amount_USD + req.param('amount');
                to.history.push(request._id, request.date);

                to.save(function(err) {
                    if (err)
                        console.log('error')
                    else
                        console.log('success')
                });

            }
            if (money == 'YUAN'){
                to.amount_YUAN = to.amount_YUAN + req.param('amount');
                to.history.push(request._id, request.date);

                to.save(function(err) {
                    if (err)
                        console.log('error')
                    else
                        console.log('success')
                });

            }
            else if (money == 'EURO')
                to.amount_EURO = to.amount_EURO + req.param('amount');
            to.history.push(request._id, request.date);

            to.save(function(err) {
                if (err)
                    console.log('error')
                else
                    console.log('success')
            });
            console.log(to.amount);
        });




        /*

        // find and update transaction_id to userDB history[]
                var query_to = {"email": req.param('email')};
                var update_to = {$push: {history: {trans_id: request._id, trans_date: request.date } }  }
                User.findOneAndUpdate(query_to, update_to, function (err, user) {
                    if (err) {
                        console.log('got an error');
                    }
                    //display user's all transaction in consloe
                    for (var i = 0; i < user.history.length; i++) {
                        console.log("transaction" + i + " = " + user.history[i].trans_date + "  of " + user.history[i].trans_id);
                    }
                });
        */

        request.save();

        res.redirect('/view/' + request._id + '/' + request.to_email)
    });

//direct to review the submitted transaction
    app.get('/view/:id/:email', function (req, res) {
        Trans.findOne({_id: req.params.id}, function (err, review) {
            if (err) {
                console.log('cannot find any transaction')
            }

            if (review.currency == "USD")
            {
                sign = "fa fa-dollar"
            }
            if (review.currency == "YUAN")
            {
                sign = "fa fa-jpy"
            }
            else if (review.currency == "EURO")
            {
                sign = "fa fa-euro"
            }
            User.findOne({email: req.params.email}, function (err, user) {
                if (err) {
                    console.log('cannot find any transaction')
                }
//var scoping here, inner can call outer
                res.render('review.ejs', {
                    review: review,
                    user: user,
                    sign: sign
                });
                console.log(user.name);
            }); //end of user.find

            console.log(review.amount + review.currency);
        });//end of trans.find

        /*
         //function of find user's name
         User.findOne({email: req.params.email}, function (err, user) {
         if (err) {
         console.log('cannot find any transaction')
         }
         res.render('review.ejs', {
         review: review
         });
         console.log(review.name);
         }); //end of user.find
         */

    });//end of review


    // direct to review history
    app.get('/view_history', function (req, res) {

        Trans.find({from_email: req.user.email}, function (err, result) {
            if (err) {
                console.log('cannot find any history for tom@gmail.com')
            }
            res.render('review_history', {
                result: result
            });
            console.log(result)
        });
    });

    // =============================================================================
// AUTHENTICATE (FIRST LOGIN) ==================================================
// =============================================================================

    // locally --------------------------------
    // LOGIN ===============================
    // show the login form
    app.get('/login', function (req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/homepage', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

    // SIGNUP =================================
    // show the signup form
    app.get('/signup', function (req, res) {
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));

//LOG OUT

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
        mongoose.connection.close();
        mongoose.connection.open('mongodb://jinlong024:jinlong024@oceanic.mongohq.com:10092/HomeTake' )
    });

}
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
}
